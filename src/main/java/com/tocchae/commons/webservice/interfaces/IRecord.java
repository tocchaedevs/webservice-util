/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.interfaces;

import java.util.Date;
import com.tocchae.commons.webservice.enums.ResponseCodeEnum;

/**
 * This interface defines the basics of an element that needs to get passed
 * throughout the lifespan of a endpoint call, the implementation of this
 * interface will also detail how this record is to be saved into a database
 *
 * @author Torti Ama-Njoku @ Tocchae
 */
public interface IRecord {

    /**
     * @return the transaction ID of this particular record
     */
    Long getTransactionId();

    /**
     * set the transactionId of this record
     *
     * @param transactionId the transaction ID of this record
     * @return this instance
     */
    IRecord setTransactionId(Long transactionId);
    
    /**
     * @return the operation ID of this record
     */
    String getOperationId();
    
    /**
     * Set the operation ID
     * 
     * @param operationId the operation ID
     * @return this instance
     */
    IRecord setOperationId(String operationId);

    /**
     * Set the response message associated with this particular record this
     * response message is used in the response sent to the database as well as
     * the response to a webservice endpoint that uses this Record
     *
     * @param response a string message sent back to whoever needs it
     * @return this instance
     */
    IRecord setResponse(String response);

    /**
     * Return the response message linked to this record
     *
     * @return the response string
     */
    String getResponse();

    /**
     * @return the attempts
     */
    int getAttempts();

    /**
     * @param attempts the attempts to set
     * @return this object
     */
    IRecord setAttempts(int attempts);

    /**
     * Set the response code associated with this particular record this
     * response code is used in the response sent to the database as well as the
     * response to a webservice endpoint that uses this Record
     *
     * @param responseCode response code enum to be set
     * @return this instance
     */
    IRecord setResponseCode(ResponseCodeEnum responseCode);

    /**
     * Returns the response code linked to this record to be used in the response
     * of the webservice request.
     *
     * @return the response code enum
     */
    ResponseCodeEnum getResponseCode();

    /**
     * The timestamp of the record
     *
     * @return timestamp
     */
    Date getTimestamp();

    /**
     * set the timestamp of the record
     *
     * @param timestamp String
     * @return this object
     */
    IRecord setTimestamp(Date timestamp);
}

