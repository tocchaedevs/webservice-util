/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.interfaces;

import com.tocchae.commons.webservice.enums.ResponseCodeEnum;

/**
 * This is the interface for all third-party service calls
 * @author Torti Ama-Njoku @ Tocchae
 */
public interface IBaseAPICall {
    
    /**
     * @param transactionEntity transaction entity associated with this API call
     * @return this instance
     */
    IBaseAPICall setTransactionEntity(IRecord transactionEntity);
    
    /**
     * Returns the transaction entity associated with the API call
     *
     * @return the transaction entity.
     */
    IRecord getTransactionEntity();
    
    /**
     * all classes implementing this interface will need to execute an endpoint retrieve the
     * response that will need to be returned to the caller.
     *
     * @return a response to be handled.
     */
    IResponse callEndpoint();
    
    /**
     * Every call that is made to an endpoint may return a code or value this method defines how the
     * incoming response message/code is translated into the standard Response codes for Airvantage
     * according to {@link ResponseCodeEnum}
     *
     * @param transactionEntity this is either a response code or message that will need to be interpreted
     * @return a response code {@link ResponseCodeEnum}
     */
    ResponseCodeEnum getResponseCode(IRecord transactionEntity);
}

