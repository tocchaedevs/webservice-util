/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.utils;

/**
 * @author Torti Ama-Njoku @ Tocchae
 */
public final class ServiceConfig {

    private boolean hasHeaders;
    private boolean isWithSSL;
    private boolean isXml;
    private boolean isJson;
    private boolean addSSLBypass;

    /**
     * default empty constructor
     */
    public ServiceConfig() {
        //empty constructor
    }

    /**
     * @return the hasHeaders
     */
    public boolean isHasHeaders() {
        return hasHeaders;
    }

    /**
     * @param hasHeaders the hasHeaders to set
     */
    public void setHasHeaders(boolean hasHeaders) {
        this.hasHeaders = hasHeaders;
    }

    /**
     * @return the isWithSSL
     */
    public boolean isIsWithSSL() {
        return isWithSSL;
    }

    /**
     * @param isWithSSL the isWithSSL to set
     */
    public void setIsWithSSL(boolean isWithSSL) {
        this.isWithSSL = isWithSSL;
    }

    /**
     * @return the isXml
     */
    public boolean isIsXml() {
        return isXml;
    }

    /**
     * @param isXml the isXml to set
     */
    public void setIsXml(boolean isXml) {
        this.isXml = isXml;
    }

    /**
     * @return the isJson
     */
    public boolean isIsJson() {
        return isJson;
    }

    /**
     * @param isJson the isJson to set
     */
    public void setIsJson(boolean isJson) {
        this.isJson = isJson;
    }

    /**
     * @return the addSSLBypass
     */
    public boolean isAddSSLBypass() {
        return addSSLBypass;
    }

    /**
     * @param addSSLBypass the addSSLBypass to set
     */
    public void setAddSSLBypass(boolean addSSLBypass) {
        this.addSSLBypass = addSSLBypass;
    }

}
