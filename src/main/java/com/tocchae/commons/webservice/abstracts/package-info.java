/**
 * The commons.abstracts package contains commons abstract classes.
 * <p>
 *
 * @since 1.0
 * @see com.tocchae
 */
package com.tocchae.commons.webservice.abstracts;

