/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.abstracts;

import org.apache.log4j.Logger;
import retrofit2.Call;
import com.tocchae.commons.webservice.entities.WebserviceTransaction;
import com.tocchae.commons.webservice.enums.ResponseCodeEnum;
import com.tocchae.commons.webservice.interfaces.IRecord;
import com.tocchae.commons.webservice.interfaces.IRequest;
import com.tocchae.commons.webservice.interfaces.IResponse;

/**
 * This class contains the execution steps for a building a retrofit service
 * factory, building a request, executing the request and returning the response
 * from the third-party.
 *
 * @author Torti Ama-Njoku @ Tocchae
 * @param <Q> request object
 * @param <R> response object
 * @param <I> retrofit interface
 */
public abstract class AbstractRetrofitAPICall<Q extends IRequest, R extends IResponse, I>
        extends AbstractBaseAPICall<R, Q> {

Q httpRequestBody;

    /**
     * Default constructor
     *
     * @param transactionEntity transaction entity associated with this API call
     */
    public AbstractRetrofitAPICall(IRecord transactionEntity) {
        super(transactionEntity);
    }

    /**
     * Initialises the service factory and implements call for retrofit endpoint
     * interface.
     *
     * @return retrofit interface
     */
    public abstract I initialiseServiceFactory();

    /**
     * Builds the retrofit request call. This should also call the
     * {@link AbstractRetrofitAPICall#buildRequest()} method to get the request
     * object to be used in creating the retrofit request Call.
     *
     * @param requestBody the retrofit request body object
     * @param retrofitApiInterface the retrofit API request interface used for
     * executing the request to the 3rd party.
     * @return retrofit request call
     */
    public abstract Call<R> getRequestCall(Q requestBody, I retrofitApiInterface);

    /**
     * Build your retrofit request object, either for json request or xml based
     * request.
     *
     * @return Q the request object
     */
    public abstract Q buildRequest();

    /**
     * This method makes a webservice call to the third-party endpoint and
     * returns the JSON/XML response object. The implementation of this method
     * should call the method {@link AbstractRetrofitAPICall#callEndpoint()}
     *
     * @param logger for logging info and errors
     * @return response object of type {@code <R>}
     *
     */
    public IResponse callEndpoint(Logger logger) {
        // Initialise the service factory
        I retrofitApiInterface = initialiseServiceFactory();

        // Build the request body
        Q requestBody = buildRequest();

        // Get the request call
        Call<R> requestCall = getRequestCall(requestBody, retrofitApiInterface);

        // Execute the HTTP request and return the response
        return executeHttpRequest(logger, requestCall, requestBody);
    }

    /**
     * This utility method executes the HTTP request call to a third-party
     *
     * @param logger for logging info and errors
     * @param requestCall prepared HTTP request that needs to be executed
     * @param requestBody the retrofit request body object
     * @return the JSON/XML response object from the request
     */
    public IResponse executeHttpRequest(Logger logger, Call<R> requestCall, Q requestBody) {

        httpRequestBody = requestBody;

        // Execute the request and get the response
        try {
            response = requestCall.execute();

            if (response != null) {
                responseCode = response.code();
                responseBody = response.body();

                    ((WebserviceTransaction) getTransactionEntity())
                            .setHttpResponseSuccess(response.isSuccessful());
                    ((WebserviceTransaction) getTransactionEntity())
                            .setGatewayResponseString(responseBody.getResponseAsString());
                
            }
        }  catch (Exception ex) {
            setExceptionResponse(ex, ex.getClass().getName(), logger);
        }

        // Set HTTP attributes in the transaction entity
        setHTTPAttributes(httpRequestBody);

        return responseBody;
    }

    /**
     * This method builds an exception response based on the exception
     *
     * @param ex the exception object that has been thrown
     * @param errorName name of this exception
     * @param logger for logging info and errors
     */
    protected void setExceptionResponse(Exception ex, String errorName, Logger logger) {
        ((WebserviceTransaction) getTransactionEntity())
                .setResponse(errorName + ": " + ex.getMessage());
        ((WebserviceTransaction) getTransactionEntity())
                .setGatewayResponseString(errorName + ": " + ex.getMessage());
        ((WebserviceTransaction) getTransactionEntity())
                .setResponseCode(ResponseCodeEnum.RESPONSE_FAILED);
        ((WebserviceTransaction) getTransactionEntity())
                .setHttpResponseSuccess(false);
        logger.fatal(null, ex);
    }
}
