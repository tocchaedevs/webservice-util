/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.webservice;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import com.tocchae.commons.webservice.utils.ServiceConfig;

/**
 * This class is used to instantiate the interfaces for the outgoing API Calls
 * To the SDP. Uses the Retrofit library
 *
 * @author Torti Ama-Njoku @ Tocchae
 */
public class ServiceFactory {

    private boolean loggingEnabled;
    private final int timeOutSeconds;
    private final ServiceConfig booleanServiceConfigs;

    /**
     * Logging disabled by default Timeout 10 seconds by default
     */
    public ServiceFactory() {
        this.loggingEnabled = false;
        this.timeOutSeconds = 10;
        booleanServiceConfigs = new ServiceConfig();
    }

    /**
     * @param loggingEnabled control logging
     */
    public ServiceFactory(boolean loggingEnabled) {
        this.loggingEnabled = loggingEnabled;
        this.timeOutSeconds = 10;
        booleanServiceConfigs = new ServiceConfig();
    }

    /**
     *
     * @param loggingEnabled control logging
     * @param timeout request timeout in seconds
     */
    public ServiceFactory(boolean loggingEnabled, int timeout) {
        this.loggingEnabled = loggingEnabled;
        this.timeOutSeconds = timeout;
        booleanServiceConfigs = new ServiceConfig();
    }

    /**
     * @return the loggingEnabled
     */
    public boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    /**
     * @param loggingEnabled the loggingEnabled to set
     */
    public void setLoggingEnabled(boolean loggingEnabled) {
        this.loggingEnabled = loggingEnabled;
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the http calls API
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @param headerList this {@link LinkedHashMap} contains the list of all the
     * http headers this in in the format key,value
     * @return S an implementation of the call defined in the retrofit API
     */
    public <S> S createServiceXml(Class<S> serviceClass, String baseUrl, Map<String, String> headerList) {

        booleanServiceConfigs.setHasHeaders(true);
        booleanServiceConfigs.setIsWithSSL(false);
        booleanServiceConfigs.setIsXml(true);
        booleanServiceConfigs.setIsJson(false);
        booleanServiceConfigs.setAddSSLBypass(true);

        return createService(serviceClass, baseUrl,
                headerList, //headers
                null, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the XML based http calls API
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceXml(Class<S> serviceClass, String baseUrl) {

        booleanServiceConfigs.setHasHeaders(false);
        booleanServiceConfigs.setIsWithSSL(false);
        booleanServiceConfigs.setIsXml(true);
        booleanServiceConfigs.setIsJson(false);
        booleanServiceConfigs.setAddSSLBypass(true);

        return createService(serviceClass, baseUrl,
                null, //headers
                null, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the http calls API. This
     * implementation does not bypass SSL certification if the protocol is
     * HTTPS.
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @param headerList this {@link LinkedHashMap} contains the list of all the
     * http headers this in in the format key,value
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceXmlNoSSLBypass(Class<S> serviceClass, String baseUrl, Map<String, String> headerList) {

        booleanServiceConfigs.setHasHeaders(true);
        booleanServiceConfigs.setIsWithSSL(false);
        booleanServiceConfigs.setIsXml(true);
        booleanServiceConfigs.setIsJson(false);
        booleanServiceConfigs.setAddSSLBypass(false);

        return createService(serviceClass, baseUrl,
                headerList, //headers
                null, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the XML based http calls API. This
     * implementation does not bypass SSL certification if the protocol is
     * HTTPS.
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceXmlNoSSLBypass(Class<S> serviceClass, String baseUrl) {

        booleanServiceConfigs.setHasHeaders(false);
        booleanServiceConfigs.setIsWithSSL(false);
        booleanServiceConfigs.setIsXml(true);
        booleanServiceConfigs.setIsJson(false);
        booleanServiceConfigs.setAddSSLBypass(false);

        return createService(serviceClass, baseUrl,
                null, //headers
                null, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the XML based http calls API with
     * SSL connection.
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @param sslSF the SSL socket factory object
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceXmlWithSSL(Class<S> serviceClass, String baseUrl,
            SSLSocketFactory sslSF) {

        booleanServiceConfigs.setHasHeaders(false);
        booleanServiceConfigs.setIsWithSSL(true);
        booleanServiceConfigs.setIsXml(true);
        booleanServiceConfigs.setIsJson(false);
        booleanServiceConfigs.setAddSSLBypass(false);

        return createService(serviceClass, baseUrl,
                null, //headers
                sslSF, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the XML based http calls API with
     * SSL connection.
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @param sslSF the SSL socket factory object
     * @param headerList this {@link LinkedHashMap} contains the list of all the
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceXmlWithSSL(Class<S> serviceClass, String baseUrl,
            SSLSocketFactory sslSF, Map<String, String> headerList) {

        booleanServiceConfigs.setHasHeaders(true);
        booleanServiceConfigs.setIsWithSSL(true);
        booleanServiceConfigs.setIsXml(true);
        booleanServiceConfigs.setIsJson(false);
        booleanServiceConfigs.setAddSSLBypass(false);

        return createService(serviceClass, baseUrl,
                headerList, //headers
                sslSF, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the json http calls API
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceJson(Class<S> serviceClass, String baseUrl) {

        booleanServiceConfigs.setHasHeaders(false);
        booleanServiceConfigs.setIsWithSSL(false);
        booleanServiceConfigs.setIsXml(false);
        booleanServiceConfigs.setIsJson(true);
        booleanServiceConfigs.setAddSSLBypass(true);

        return createService(serviceClass, baseUrl,
                null, //headers
                null, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the json http calls API
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @param headerList this {@link LinkedHashMap} contains the list of all the
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceJson(Class<S> serviceClass, String baseUrl,
            Map<String, String> headerList) {

        booleanServiceConfigs.setHasHeaders(true);
        booleanServiceConfigs.setIsWithSSL(false);
        booleanServiceConfigs.setIsXml(false);
        booleanServiceConfigs.setIsJson(true);
        booleanServiceConfigs.setAddSSLBypass(true);

        return createService(serviceClass, baseUrl,
                headerList, //headers
                null, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the json http calls API. This
     * implementation does not bypass SSL certification if the protocol is
     * HTTPS.
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceJsonNoSSLBypass(Class<S> serviceClass, String baseUrl) {

        booleanServiceConfigs.setHasHeaders(false);
        booleanServiceConfigs.setIsWithSSL(false);
        booleanServiceConfigs.setIsXml(false);
        booleanServiceConfigs.setIsJson(true);
        booleanServiceConfigs.setAddSSLBypass(false);

        return createService(serviceClass, baseUrl,
                null, //headers
                null, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the json http calls API. This
     * implementation does not bypass SSL certification if the protocol is
     * HTTPS.
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @param headerList this {@link LinkedHashMap} contains the list of all the
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceJsonNoSSLBypass(Class<S> serviceClass, String baseUrl,
            Map<String, String> headerList) {

        booleanServiceConfigs.setHasHeaders(true);
        booleanServiceConfigs.setIsWithSSL(false);
        booleanServiceConfigs.setIsXml(false);
        booleanServiceConfigs.setIsJson(true);
        booleanServiceConfigs.setAddSSLBypass(false);

        return createService(serviceClass, baseUrl,
                headerList, //headers
                null, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the JSON based http calls API with
     * SSL connection.
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @param sslSF the SSL socket factory object
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceJsonWithSSL(Class<S> serviceClass, String baseUrl,
            SSLSocketFactory sslSF) {

        booleanServiceConfigs.setHasHeaders(false);
        booleanServiceConfigs.setIsWithSSL(true);
        booleanServiceConfigs.setIsXml(false);
        booleanServiceConfigs.setIsJson(true);
        booleanServiceConfigs.setAddSSLBypass(false);

        return createService(serviceClass, baseUrl,
                null, //headers
                sslSF, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates a service that takes an API interface and a URL and returns a
     * value. this implemetation is used for the JSON based http calls API with
     * SSL connection.
     *
     * @param <S> this takes in the retrofit interface you wish you execute
     * @param serviceClass this takes in the retrofit interface you wish you
     * execute
     * @param baseUrl this is the base URL of the service, the URL to the SDP
     * @param sslSF the SSL socket factory object
     * @param headerList this {@link LinkedHashMap} contains the list of all the
     * @return S an implementation of the call defined in the interface
     */
    public <S> S createServiceJsonWithSSL(Class<S> serviceClass, String baseUrl,
            SSLSocketFactory sslSF, Map<String, String> headerList) {

        booleanServiceConfigs.setHasHeaders(true);
        booleanServiceConfigs.setIsWithSSL(true);
        booleanServiceConfigs.setIsXml(false);
        booleanServiceConfigs.setIsJson(true);
        booleanServiceConfigs.setAddSSLBypass(false);

        return createService(serviceClass, baseUrl,
                headerList, //headers
                sslSF, //SSL
                booleanServiceConfigs
        );
    }

    /**
     * Creates the retrofit service
     *
     * @param <S> service class type
     * @param serviceClass service class to be retrofitted
     * @param baseUrl service endpoint URL
     * @param headerList list of headers to be passed
     * @param sslSF SSL socket factory certificate to be set
     * @param booleanServiceConfigs contains the boolean service factory
     * configuration settings
     *
     * HTTPS protocol
     * @return returns the retrofitted service class
     */
    private <S> S createService(Class<S> serviceClass, String baseUrl,
            Map<String, String> headerList,
            SSLSocketFactory sslSF, ServiceConfig booleanServiceConfigs) {
        OkHttpClient.Builder okHttpClientBuilder = HTTPClient
                .getInstance(this.loggingEnabled, timeOutSeconds)
                .getOkHttpClient()
                .newBuilder();
        
        if (this.loggingEnabled) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClientBuilder.addInterceptor(loggingInterceptor);
        }
        
        if (booleanServiceConfigs.isHasHeaders()) {
            //add the http headers to the OK builder
            addHeaders(okHttpClientBuilder, headerList);
        } else {
            okHttpClientBuilder.addInterceptor((Interceptor.Chain chain) -> {
                Request.Builder builder = chain.request().newBuilder();
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            });
        }

        if (booleanServiceConfigs.isIsWithSSL()) {
            okHttpClientBuilder.sslSocketFactory(sslSF);
        } else {
            //Bypass HTTPS if necessary
            if (booleanServiceConfigs.isAddSSLBypass()) {
                addSSLBypass(okHttpClientBuilder, baseUrl);
            }
        }

        Retrofit.Builder retrofitBuilder;
        retrofitBuilder = new Retrofit.Builder()
                .baseUrl(baseUrl);

        if (booleanServiceConfigs.isIsXml()) {
            addXmlConverter(retrofitBuilder);
        } else if (booleanServiceConfigs.isIsJson()) {
            addJacksonConverter(retrofitBuilder);
        }

        retrofitBuilder = retrofitBuilder.client(okHttpClientBuilder.build());
        Retrofit retrofit = retrofitBuilder.build();
        return retrofit.create(serviceClass);
    }

    /**
     * Add a GSON Converter for retrofit JSON responses
     *
     * @param builder the retrofit builder to add the converter to
     * @return return the modified retrofit builder
     */
    private Retrofit.Builder addJacksonConverter(Retrofit.Builder builder) {
        return builder.addConverterFactory(JacksonConverterFactory.create());
    }

    /**
     * Add an XML Converter for retrofit XML responses
     *
     * @param builder the retrofit builder to add the converter to
     * @return return the modified retrofit builder
     */
    private Retrofit.Builder addXmlConverter(Retrofit.Builder builder) {
        return builder.addConverterFactory(SimpleXmlConverterFactory.create());
    }

    /**
     * Adds the headers to the HTTP client builder
     *
     * @param okbuilder HTTP Client builder to add the headers to
     * @param headerList list of headers to add
     */
    private void addHeaders(OkHttpClient.Builder okbuilder, Map<String, String> headerList) {
        //add the http headers to the OK builder
        okbuilder.addInterceptor((Interceptor.Chain chain) -> {
            Request.Builder builder = chain.request().newBuilder();
            headerList.entrySet().forEach(entry -> {
                String key = entry.getKey();
                String value = entry.getValue();
                builder.addHeader(key, value);
            });
            Request newRequest = builder.build();
            return chain.proceed(newRequest);
        });
    }

    /**
     * Adds the SSL bypass for a specified URL if the URL is https
     *
     * @param okbuilder HTTP Client builder to add the bypass to
     * @param baseUrl URL
     */
    private void addSSLBypass(OkHttpClient.Builder okbuilder, String baseUrl) {
        //Add bypass SSL if URL is HTTPS
        if (isHttpsUrl(baseUrl)) {
            try {
                // Install the all-trusting trust manager
                final TrustManager[] trustAllCerts = getTrustManagerAllCerts();
                final SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, trustAllCerts, new SecureRandom());
                // Create an ssl socket factory with our all-trusting manager
                SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
                okbuilder.sslSocketFactory(sslSocketFactory);
                //Use a lamda expression because HostnameVerifier is a functional interface this is actually
                //one of the perfect use cases for a lamda expressions. During complie the jdk will search for 
                //all the functional intefaces that meet the parameters as given, in this case HostnameVerifier.
                //an implemtation of HostnameVerifier needs to overide the  
                //public boolean verify(String hostname, SSLSession session) in this case the method simply 
                //returns true as such.
                //This will then need to be added as an anonymous class in okbuilder.hostnam}Verifier method
                //witch just looks oevery complex it can be simplified thus.
                okbuilder.hostnameVerifier((String hostname, SSLSession session) -> true);
            } catch (NoSuchAlgorithmException | KeyManagementException ex) {
                Logger.getLogger(ServiceFactory.class.getName()).log(Level.FATAL, null, ex);
            }
        }
    }

    /**
     * Generate Trust manager for all certificates
     *
     * @return trust manager
     */
    private TrustManager[] getTrustManagerAllCerts() {
        // Create a trust manager that does not validate certificate chains
        return new TrustManager[]{
            new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        X509Certificate[] chain, String authType) throws CertificateException {
                    //Not Implemented
                }

                @Override
                public void checkServerTrusted(
                        X509Certificate[] chain, String authType) throws CertificateException {
                    //Not Implemented
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }
        };
    }

    /**
     * Returns true if the url is SSL enabled
     *
     * @param url URL to check
     * @return true if the url is SSL enabled
     */
    private boolean isHttpsUrl(String url) {
        return (null != url) && (url.length() > 7) && "https://".equalsIgnoreCase(url.substring(0, 8));
    }
}
