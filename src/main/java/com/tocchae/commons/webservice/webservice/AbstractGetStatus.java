/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.webservice;

import com.tocchae.commons.webservice.response.Endpoint;
import com.tocchae.commons.webservice.response.GetStatusResponse;
import com.tocchae.commons.webservice.response.WarDetail;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * Returns the status of the endpoint. 
 *
 * @author Torti Ama-Njoku
 */
public abstract class AbstractGetStatus extends AbstractWebservice {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(AbstractGetStatus.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods. This method requires the parsing in of a list of {@link Endpoint} it has been proposed
     * that the best way to store this is to store the values of each of these attributes in a table as each endpoint
     * is sucessfully 
     * @param request servlet request
     * @param response servlet response
     * @param endpointDetail a list of each endpoints status
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void processRequestCall(HttpServletRequest request, HttpServletResponse response,
            List<Endpoint> endpointDetail)
            throws ServletException, IOException {
        
        WarDetail warDetail = getWarDetail();
        GetStatusResponse avGetStatusResponse = new GetStatusResponse().
                setEndpoints(endpointDetail).
                setWarDetail(warDetail);         

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            out.println(avGetStatusResponse.getResponseAsString());
        }
    }

    /**
     * this method builds the War detail object based on the seeing in the
     * manefest as well as the db version of this deploy
     *
     * @return will return an instance of {@link WarDetail}
     */
    private WarDetail getWarDetail() {
        WarDetail warDetail = new WarDetail();
        try {
            InputStream input = getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF");
            Manifest manifest = new Manifest(input);
            Attributes attr = manifest.getMainAttributes();

            warDetail.
                    setBuildDate(attr.getValue("Version-Build-Date")).
                    setName(attr.getValue("Microservice-Name")).
                    setWarVersion(attr.getValue("Microservice-Version"));
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return warDetail;
    }

}
