/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.webservice;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * This class creates a singleton OKHTTPClient based on various factors such as
 * the following -whether logging should be enabled or not -the timeout of the
 * connection -
 *
 * @author Torti Ama-Njoku @ Tocchae
 */
public final class HTTPClient {

    private static OkHttpClient okHttpClient;
    private static boolean loggingEnabled;
    private static int timeOutSeconds;

    /**
     * Private default constructor
     */
    private HTTPClient() {
        if (loggingEnabled) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(timeOutSeconds, TimeUnit.SECONDS)
                    .connectTimeout(timeOutSeconds, TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor)
                    .build();
        } else {
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(timeOutSeconds, TimeUnit.SECONDS)
                    .connectTimeout(timeOutSeconds, TimeUnit.SECONDS)
                    .build();
        }

    }

    /**
     * Singleton instantiation
     */
    private static final class HttpClientLoader {

        private static final HTTPClient INSTANCE = new HTTPClient();

        /**
         * Private default constructor
         */
        private HttpClientLoader() {
            // private contructor to bypass instantiation
        }

        /**
         * Returns getter for Singleton instance
         *
         * @return DBResource instance
         */
        public static HTTPClient getINSTANCE() {
            return INSTANCE;
        }
    }

    /**
     * Get the static INSTANCE of HTTPClient
     * <p>
     * Note: Although we share the okHttpClient, each subsequent call via
     * retrofit will still have its own Connection.
     *
     * @param loggingEnabled this will determine if the okHttpClient will have
     * either logging enabled or not note that this operator will only work once
     * changing its state will have no impact on the singleton
     * @param timeOutSeconds this will set the timeous for the okHttpClient note that this operator 
     * will only work once changing its state will have no impact on the singleton
     * @return Instance of HTTPClient
     */
    public static HTTPClient getInstance(boolean loggingEnabled, int timeOutSeconds) {
        HTTPClient.loggingEnabled = loggingEnabled;
        HTTPClient.timeOutSeconds = timeOutSeconds;
        return HttpClientLoader.getINSTANCE();
    }

    /**
     * @return the okHttpClient
     */
    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

}
