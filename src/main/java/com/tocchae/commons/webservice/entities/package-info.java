/**
 * The commons.entities package contains commons entity classes, usually transaction
 * entities.
 * <p>
 *
 * @since 1.0
 * @see com.tocchae
 */
package com.tocchae.commons.webservice.entities;

