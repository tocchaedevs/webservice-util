/**
 * The commons.response package contains webservice or socket response objects
 * used in third party integrations.
 * <p>
 *
 * @since 1.0
 */
package com.tocchae.commons.webservice.response;

