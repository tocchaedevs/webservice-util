/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tocchae.commons.webservice.interfaces.IResponse;
import com.tocchae.commons.webservice.utils.SerializerUtil;

/**
 *
 * @author Torti Ama-Njoku
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultResponseBody implements IResponse {

    private boolean success;
    private DefaultResponseBodyDetail detail;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * @return the detail
     */
    public DefaultResponseBodyDetail getDetail() {
        return detail;
    }

    /**
     * @param detail the detail to set
     */
    public void setDetail(DefaultResponseBodyDetail detail) {
        this.detail = detail;
    }

    @Override
    @JsonIgnore
    public String getResponseAsString() {
        return SerializerUtil.serialiseToJson(this);
    }
}
