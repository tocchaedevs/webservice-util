package com.tocchae.commons.webservice.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * the pojo used to set the endpoint values
 *
 * @author Torti Ama-Njoku @ Tocchae
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Endpoint {

    @JsonProperty("endpointName")
    private String endpointName;
    @JsonProperty("lastExecuted")
    private String lastExecuted;
    @JsonProperty("status")
    private String status;
    @JsonProperty("statusCount")
    private int statusCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties;

    /**
     * Default Constructor
     */
    public Endpoint() {
        additionalProperties = new HashMap<>();
    }

    /**
     * the name of the endpoint
     *
     * @return the name of the endpoint
     */
    @JsonProperty("endpointName")
    public String getEndpointName() {
        return endpointName;
    }

    /**
     * set the endpoint name
     *
     * @param endpointName the name of the end point
     * @return this object
     */
    @JsonProperty("endpointName")
    public Endpoint setEndpointName(String endpointName) {
        this.endpointName = endpointName;
        return this;
    }

    /**
     * when last the endpoint was called
     *
     * @return when last the endpoint was called
     */
    @JsonProperty("lastExecuted")
    public String getLastExecuted() {
        return lastExecuted;
    }

    /**
     * set when the endpoint was last called
     *
     * @param lastExecuted set when the endpoint was last called
     * @return this object
     */
    @JsonProperty("lastExecuted")
    public Endpoint setLastExecuted(String lastExecuted) {
        this.lastExecuted = lastExecuted;
        return this;
    }

    /**
     * the status of the endpoint the last time it was called
     *
     * @return the status of the endpoint the last time it was called
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * set the status of the endpoint that has just been called
     *
     * @param status the status of the endpoint that has just been called
     * @return this object
     */
    @JsonProperty("status")
    public Endpoint setStatus(String status) {
        this.status = status;
        return this;
    }

    /**
     * a count of the statuses
     *
     * @return a count of the statuses
     */
    @JsonProperty("statusCount")
    public int getStatusCount() {
        return statusCount;
    }

    /**
     * set the count of the status
     *
     * @param statusCount the count of the status
     * @return this object
     */
    @JsonProperty("statusCount")
    public Endpoint setStatusCount(int statusCount) {
        this.statusCount = statusCount;
        return this;
    }

    /**
     * return any additional parameters
     *
     * @return any addtional parameters
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * set any addtional parameters
     *
     * @param name the name of the parameter
     * @param value the value of said parameter
     * @return this object
     */
    @JsonAnySetter
    public Endpoint setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
