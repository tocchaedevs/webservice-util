/**
 * This package contains all the base classes that are used to validate incoming messages via
 * a sevelet. Thus contains a interface that defines fuctionality an abstract class that contains the 
 * common attributes and 2 classes, onefor SOAP and another for JSON validation.
 * <p>
 * Date-, Database-, and Text utilities and parsers can be accessed from this package.
 *
 * @since 1.0
 */
package com.tocchae.commons.webservice.validate;
