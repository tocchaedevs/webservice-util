/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tocchae.commons.webservice.validate;

import com.tocchae.commons.webservice.interfaces.IRequest;

/**
 * this interface defines the basic fuctionality of how to validate an incoming
 * message
 *
 * @author Torti Ama-Njoku
 * @param <Q> request object
 */
public interface Validation<Q extends IRequest> {

    /**
     * this method is responsible for validating the incoming request to ensure
     * that it meets the requirements
     *
     * @param envelopeClass this is the expected class that should be parsed
     * @param requestString this is the request json string
     * @return true if the validation is sucessful
     */
    boolean validate(Class<Q> envelopeClass, String requestString);

    /**
     * this method will return the reason message if the validation fails
     *
     * @return a String reason message
     */
    String getMessage();

    /**
     * @param message this is used to set the message if the validation is
     * unsucessfull
     */
    void setMessage(String message);

    /**
     * @return if the validation is sucessful this will return true if not
     * false;
     */
    boolean isValid();

    /**
     * @param valid sets the validation state of the validation true/false
     */
    void setValid(boolean valid);
}

